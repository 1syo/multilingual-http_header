# -*- encoding: utf-8 -*-
require File.expand_path('../lib/multilingual-http_header/version', __FILE__)

Gem::Specification.new do |gem|
  gem.name          = "multilingual-http_header"
  gem.version       = Multilingual::HttpHeader::VERSION

  gem.authors       = ["TAKAHASHI Kazunari"]
  gem.email         = ["takahashi@1syo.net"]
  gem.description   = %q{ Rails Plugin, Automatically set the locale from HTTP_ACCEPT_LANGUAGE header.}
  gem.summary       = %q{ Rails Plugin, Automatically set the locale from HTTP_ACCEPT_LANGUAGE header.}
  gem.homepage      = "https://github.com/1syo/multilingual-http_header"

  gem.files         = `git ls-files`.split($\)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]

  gem.licenses = ['MIT']

  gem.add_dependency 'http_accept_language', [">= 2.0.0.pre"]
  gem.add_dependency 'i18n', [">= 0.6.0"]

  gem.add_development_dependency 'bundler', ['>= 1.0.0']
  gem.add_development_dependency 'rake', ['>= 0']
  gem.add_development_dependency 'rdoc', ['>= 0']
  gem.add_development_dependency 'rspec', ['>= 2.1.0']
  gem.add_development_dependency 'rails', [">= 3.2.9"]
  gem.add_development_dependency 'rspec-rails', ['>= 2.1.0']
  gem.add_development_dependency 'capybara', ['>= 2.0.0']
  gem.add_development_dependency 'pry', ['>= 0']
end
