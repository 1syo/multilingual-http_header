# Multilingual::HttpHeader

[![Build Status](https://secure.travis-ci.org/1syo/multilingual-http_header.png?branch=master)](https://travis-ci.org/1syo/multilingual-http_header)
[![Dependency Status](https://gemnasium.com/1syo/multilingual-http_header.png)](https://gemnasium.com/1syo/multilingual-http_header)
[![Code Climate](https://codeclimate.com/badge.png)](https://codeclimate.com/github/1syo/multilingual-http_header)

Rails Plugin, Automatically set the locale from HTTP_ACCEPT_LANGUAGE header.

## Installation

Add this line to your application's Gemfile:

    gem 'multilingual-http_header'

And then execute:

    $ bundle

## Usage

Add your ApplicationController

    class ApplicationController < ActionController::Base
      before_filter Multilingual::HttpHeader
    end

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
