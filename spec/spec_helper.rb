$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..', 'lib'))
$LOAD_PATH.unshift(File.dirname(__FILE__))

require 'bundler/setup'
Bundler.require

ENV["RAILS_ENV"] ||= 'test'

require 'pry'
require 'fake_app/rails_app'
require 'rspec/rails'
require 'capybara'
require 'capybara/rails'
require 'capybara/rspec'

RSpec.configure do |config|
  config.mock_with :rspec
  Capybara.default_host = 'http://example.org'
  Capybara.current_driver = :rack_test
end
