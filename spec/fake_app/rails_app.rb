require 'action_controller/railtie'
require 'action_view/railtie'

app = Class.new(Rails::Application)
app.config.secret_token = '3b7cd727ee24e8444053437c36cc66c4'
app.config.session_store :cookie_store, :key => '_myapp_session'
app.config.active_support.deprecation = :log
app.config.root = File.dirname(__FILE__)
app.config.i18n.default_locale = :en
app.config.i18n.available_locales = [:en, :ja, :"zh-CN"]

Rails.backtrace_cleaner.remove_silencers!
app.initialize!

# routes
app.routes.draw do
  resources :users
end

# controllers
class ApplicationController < ActionController::Base
  before_filter Multilingual::HttpHeader
end

class UsersController < ApplicationController
  def index
    render text: "users"
  end
end
