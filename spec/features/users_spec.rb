require 'spec_helper'

describe UsersController do
  let!(:available_locales) { [:ja, :en, :"zh-CN", :ko] }
  shared_context "visit users_path with accept_language header" do
    before do
      I18n.available_locales = available_locales
      page.driver.header 'ACCEPT_LANGUAGE', accept_language
      visit users_path
    end
  end

  context "call Multilingual::HttpHeader::Filter class" do
    before do
      Multilingual::HttpHeader.should_receive(:filter) { I18n.locale = :en }
      visit users_path
    end
    it { I18n.locale.should eq :en }
  end

  context "http accept language with :ja" do
    let!(:accept_language) { 'ja,zh-cn;q=0.8,ko-kr;q=0.3' }
    include_context "visit users_path with accept_language header"
    it { I18n.locale.should eq :ja }
  end

  context "secondary http accept language with :ko-kr" do
    let!(:accept_language) { 'fr,ko-kr;q=0.8,zh-cn;q=0.3' }
    include_context "visit users_path with accept_language header"
    it { I18n.locale.should eq :ko }
  end

  context "http accept language with :zh-CN" do
    let!(:accept_language) { 'zh-cn,ja;q=0.8,ko-kr;q=0.3' }
    include_context "visit users_path with accept_language header"
    it { I18n.locale.should eq :"zh-CN" }
  end

  context "http accept language with :zh-TW" do
    let!(:accept_language) { 'zh-tw,ja;q=0.8,ko-kr;q=0.3' }
    include_context "visit users_path with accept_language header"
    it { I18n.locale.should eq :"zh-CN" }
  end

  context "http accept language with :zh" do
    let!(:accept_language) { 'zh,ja;q=0.8,ko-kr;q=0.3' }
    include_context "visit users_path with accept_language header"
    it { I18n.locale.should eq :"zh-CN" }
  end

  context 'unsupported locale' do
    let!(:accept_language) { :fr }
    include_context "visit users_path with accept_language header"
    it { I18n.locale.should eq :en }
  end
end
