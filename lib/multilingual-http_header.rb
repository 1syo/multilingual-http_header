require 'http_accept_language/parser'
require 'multilingual-http_header/version'

module Multilingual
  module HttpHeader
    def self.filter(controller)
      I18n.locale = detect_locale(controller.request.env['HTTP_ACCEPT_LANGUAGE']) || default_locale
    end

    def self.detect_locale(accept_language)
      HttpAcceptLanguage::Parser.new(accept_language).language_region_compatible_from(available_locales)
    end

    def self.available_locales
      I18n.available_locales.map(&:to_s)
    end

    def self.default_locale
      I18n.default_locale
    end
  end
end
